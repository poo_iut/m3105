package fr.dutinfo.ctrltp.a;

public abstract class Cake implements Cloneable{

	public Cake prepareCake() {
		try {
			return (Cake) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
}
