package fr.dutinfo.ctrltp.a;

public class CheeseCake extends Cake{

	private String sugar;
	private String butter;
	private String cheese;
	private String name;
	
	public CheeseCake() {}
	
	@Override
	public CheeseCake prepareCake() {
		return (CheeseCake) super.prepareCake();
	}
	
	public void addSugar(String sugar) {
		this.sugar = sugar;
	}

	public void addButter(String butter) {
		this.butter = butter;
	}

	public void addCheese(String cheese) {
		this.cheese = cheese;
	}

	public void addName(String name) {
		this.name = name;
	}

	public void setCustomerName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Sugar:" + sugar + ", Butter:" + butter + ", Name: "+name+", Cheese:" + cheese;
	}
	
	
	
	
}
