package fr.dutinfo.ctrltp.a;

public class RecapDay {

	private static RecapDay instance;

	public static synchronized RecapDay getJournalOfTheDay() {
		if (instance == null) {
			instance = new RecapDay();
		}
		return instance;
	}

	private StringBuilder stringBuilder;
	
	private RecapDay() {
		this.stringBuilder = new StringBuilder("Recap Of the Day: "+"\n");
	}

	public void addNewOrder(CheeseCake cake) {
		this.stringBuilder.append(cake.toString()+" "+ "\n");
	}

	@Override
	public String toString() {
		return this.stringBuilder.toString();
	}
	
}
