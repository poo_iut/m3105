package fr.dutinfo.ctrltp.b;

public abstract class Item implements Cloneable{

	private String nom;
	private float prix;
	
	public Item(String nom, float prix) {
		this.nom = nom;
		this.prix = prix;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "[nom=" + nom + ", prix=" + prix + "]";
	}
	
	@Override
	protected Item clone() {
		try {
			return (Item) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
}
