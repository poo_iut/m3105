package fr.dutinfo.ctrltp.b;

public class Menu implements Cloneable{

	public static Menu initializeMenu(Principal principal, Accompagnement accompagnement, Boisson boisson) {
		Menu menu = new Menu();
		menu.setPrincipal(principal);
		menu.setAccompagnement(accompagnement);
		menu.setBoisson(boisson);
		return menu;
	}

	private Principal principal;
	private Accompagnement accompagnement;
	private Boisson boisson;
	
	private Menu() {}
	
	public Principal getPrincipal() {
		return principal;
	}
	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}
	public Accompagnement getAccompagnement() {
		return accompagnement;
	}
	public void setAccompagnement(Accompagnement accompagnement) {
		this.accompagnement = accompagnement;
	}
	public Boisson getBoisson() {
		return boisson;
	}
	public void setBoisson(Boisson boisson) {
		this.boisson = boisson;
	}
	
	@Override
	public String toString() {
		return "Menu [principal=" + principal + ", accompagnement=" + accompagnement + ", boisson=" + boisson + "]";
	}

	@Override
	public Menu clone() {
		try {
			Menu clone = (Menu) super.clone();
			clone.principal = (Principal) this.principal.clone();
			clone.boisson = (Boisson) this.boisson.clone();
			clone.accompagnement = (Accompagnement) this.accompagnement.clone();
			return clone;
		}catch (Exception e) {
			return null;
		}
	}
	
	
}
