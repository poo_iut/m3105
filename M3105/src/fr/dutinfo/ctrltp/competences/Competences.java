package fr.dutinfo.ctrltp.competences;

import java.util.HashMap;
import java.util.Map;

public abstract class Competences {

	private Competences suivant;
	
	protected Map<String, String> competences;
	
	public Competences() {
		this.competences = new HashMap<>();
	}
	
	public void rechercheCompetence(String competence) {
		boolean contient = false;
		for(String competences : this.competences.values()) {
			if(competences.toLowerCase().contains(competence.toLowerCase())) {
				System.out.println(competences);
				contient = true; 
			}
		}
		if(!contient) {
			System.out.println("-");
		}else {
			System.out.println(" ");
		}
		if(this.suivant != null) {
			this.suivant.rechercheCompetence(competence);
		}
	}
	
	
	public void rechercherDomaine(String domaine) {
		if(this.competences.containsKey(domaine.toLowerCase())) {
			System.out.println(this.competences.get(domaine));
		}else {
			System.out.println("-");
		}
		if(this.suivant != null) {
			this.suivant.rechercherDomaine(domaine);
		}
	}

	public void setSuivant(Competences suivant) {
		this.suivant = suivant;
	}

	
}
