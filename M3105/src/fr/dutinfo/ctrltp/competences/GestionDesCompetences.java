package fr.dutinfo.ctrltp.competences;

import fr.dutinfo.ctrltp.competences.compt.CompetencesCommunes;
import fr.dutinfo.ctrltp.competences.compt.CompetencesDepartement;
import fr.dutinfo.ctrltp.competences.compt.CompetencesEtat;
import fr.dutinfo.ctrltp.competences.compt.CompetencesRegion;

public class GestionDesCompetences {
	
	private final CompetencesCommunes cc;
	private final CompetencesDepartement cd;
	private final CompetencesRegion cr;
	private final CompetencesEtat ce;
	
	public GestionDesCompetences() {
		this.cc = new CompetencesCommunes();
		this.cd = new CompetencesDepartement();
		this.cr = new CompetencesRegion();
		this.ce = new CompetencesEtat();
	}
	
	public void rechercherUneCompetenceDuBasVersLeHaut(String competence) {
		System.out.println("Recherche de la responsabilité de la compétence " + competence);
		this.cc.setSuivant(this.cd);
		this.cd.setSuivant(this.cr);
		this.cr.setSuivant(this.ce);
		this.cc.rechercheCompetence(competence);
	}

	public void rechercherUneCompetenceDuHautVersLeBas(String competence) {
		this.ce.setSuivant(this.cr);
		this.cr.setSuivant(this.cd);
		this.cd.setSuivant(this.cc);
		this.ce.rechercheCompetence(competence);
	}
	
	public void afficherCompetencesDomaineDuBasVersLeHaut(String domaine) {
		System.out.println("afficher les compétences du domaine " + domaine);
		this.cc.setSuivant(this.cd);
		this.cd.setSuivant(this.cr);
		this.cr.setSuivant(this.ce);
		
		this.cc.rechercherDomaine(domaine);
	}
	
	public static void main(String[] args) {
		GestionDesCompetences france = new GestionDesCompetences();
		
		france.rechercherUneCompetenceDuBasVersLeHaut("police");
		System.out.println("====");
		france.rechercherUneCompetenceDuBasVersLeHaut("aérodrome");
		System.out.println("====");
		france.rechercherUneCompetenceDuBasVersLeHaut("handicap");
		System.out.println("====");
		france.rechercherUneCompetenceDuHautVersLeBas("port");
		System.out.println("====");
		france.rechercherUneCompetenceDuHautVersLeBas("université");
		System.out.println("====");
		france.afficherCompetencesDomaineDuBasVersLeHaut("Enseignement");
		System.out.println("====");
		france.afficherCompetencesDomaineDuBasVersLeHaut("Urbanisme");
	}

}
