package fr.dutinfo.ctrltp.competences.compt;

import fr.dutinfo.ctrltp.competences.Competences;

public class CompetencesCommunes extends Competences {

	public CompetencesCommunes() {
		super();
		super.competences.put("Enseignement","écoles (batiments)");
		super.competences.put("Culture et vie sociale","éducation, création, bibliothéques, musées, archives");
		super.competences.put("Enfance","créches, centres de loisirs");
		super.competences.put("sports et loisirs","équipements et subventions, tourisme");
		super.competences.put("Action sociale et médico-sociale","CCAS : centre communal déaction sociale");
		super.competences.put("Urbanisme","plan local déurbanisme, schéma de cohérence territoriale, permis de construire, zone déaménagement concerté");
		super.competences.put("Aménagement du territoir","Schéma régional (avis, approbation)");
		super.competences.put("Environnement","Espaces naturels, collecte et traitement des déchets, Eau (distribution, assainissement), énergie (distribution)");
		super.competences.put("Grands équipements","Ports de plaisance, Aérodromes");
		super.competences.put("Développement économique","Aides indirectes");
		super.competences.put("Sécurité","Police municipale, Circulation et stationnement, Prévention de la délinquance");
	}

	@Override
	public void rechercheCompetence(String competence) {
		System.out.println("Communes:");
		super.rechercheCompetence(competence);
	}
	
	@Override
	public void rechercherDomaine(String domaine) {
		System.out.println("Commune : ");
		super.rechercherDomaine(domaine);
	}
	
}
