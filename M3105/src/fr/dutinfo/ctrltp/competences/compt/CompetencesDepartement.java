package fr.dutinfo.ctrltp.competences.compt;

import fr.dutinfo.ctrltp.competences.Competences;

public class CompetencesDepartement extends Competences {

	public CompetencesDepartement() {
		super();
		super.competences.put("Enseignement","Colléges (bétiments, personnels ouvriers, techniciens et de service)");
		super.competences.put("Culture et vie sociale","éducation, création, bibliothéques, musées, archives");
		super.competences.put("Action sociale et médico-sociale","protection maternelle et infantile, aide sociale é léenfance");
		super.competences.put("Aménagement du territoir","Schéma régional (avis, approbation)");
		super.competences.put("Environnement","Espaces naturels, Déchets (plan départemental), participation au schéma déaménagement et de gestion des eaux");
		super.competences.put("Grands équipements","Ports maritimes, de commerce et de péche, Aérodromes");
		super.competences.put("Développement économique","Aides indirectes");
		super.competences.put("Sécurité","Circulation, Prévention de la délinquance, Incendie et secours");
	}

	@Override
	public void rechercheCompetence(String competence) {
		System.out.println("Departement:");
		super.rechercheCompetence(competence);
	}
	
	@Override
	public void rechercherDomaine(String domaine) {
		System.out.print("Departement : ");
		super.rechercherDomaine(domaine);
	}
	
}
