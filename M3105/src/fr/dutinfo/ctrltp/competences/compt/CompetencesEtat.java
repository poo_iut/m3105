package fr.dutinfo.ctrltp.competences.compt;

import fr.dutinfo.ctrltp.competences.Competences;

public class CompetencesEtat extends Competences {

	public CompetencesEtat() {
		super();
		super.competences.put("Formation Professionnelle et Apprentissage","Définition de la politique nationale et mise en éuvre pour certains publics");
		super.competences.put("Enseignement","Universités (bétiments, personnel), Politique éducative");
		super.competences.put("Culture et vie sociale","patrimoine, éducation, création, bibliothéques, musées, archives");
		super.competences.put("sports et loisirs","formation, subventions, tourisme");
		super.competences.put("Action sociale et médico-sociale","allocation déadulte handicapé, centre déhébergement et de réinsertion sociale");
		super.competences.put("Urbanisme","projet déintérét général, opérations déintérét national, directive territoriale déaménagement");
		super.competences.put("Aménagement du territoir","politique d'aménagement du territoir, contrat de projet état/région");
		super.competences.put("Environnement","Espaces naturels, Parcs Nationaux, schéma déaménagement et de gestion des eaux, énergie");
		super.competences.put("Grands équipements","Ports autonomes et déintérét national, Voies navigables, Aérodromes");
		super.competences.put("Développement économique","Politique économique");
		super.competences.put("Sécurité","Police générale et polices spéciales");
	}
	
	@Override
	public void rechercheCompetence(String competence) {
		System.out.println("Etats:");
		super.rechercheCompetence(competence);
	}
	
	@Override
	public void rechercherDomaine(String domaine) {
		System.out.print("Etat : ");
		super.rechercherDomaine(domaine);
	}

}
