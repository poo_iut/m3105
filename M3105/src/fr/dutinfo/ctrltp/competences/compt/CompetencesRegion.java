package fr.dutinfo.ctrltp.competences.compt;

import fr.dutinfo.ctrltp.competences.Competences;

public class CompetencesRegion extends Competences {

	public CompetencesRegion() {
		super();
		super.competences.put("Formation Professionnelle et Apprentissage","Définition de la politique régionale et mise en éuvre");
		super.competences.put("Enseignement","Lycées (bétiments, personnels ouvriers, techniciens et de service)");
		super.competences.put("Culture et vie sociale","patrimoine, éducation, création, bibliothéques, musées, archives");
		super.competences.put("sports et loisirs","subventions, tourisme");
		super.competences.put("Aménagement du territoir","Schéma régional (élaboration), contrat de projet état/région");
		super.competences.put("Environnement","Espaces naturels, Parcs Régionaux, participation au schéma déaménagement et de gestion des eaux");
		super.competences.put("Grands équipements","Ports fluviaux, Aérodromes");
		super.competences.put("Développement économique","Aides directes et indirectes");
	}

	@Override
	public void rechercheCompetence(String competence) {
		System.out.println("Region:");
		super.rechercheCompetence(competence);
	}
	
	@Override
	public void rechercherDomaine(String domaine) {
		System.out.print("Region : ");
		super.rechercherDomaine(domaine);
	}
	
}
