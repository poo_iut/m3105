package fr.dutinfo.tp.banque;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Journal {

	private static Map<String, Journal> multiton = new HashMap<>();
	
	public static synchronized Journal getInstance(String key) {
		if(!multiton.containsKey(key)) {
			multiton.put(key, new Journal());
		}
		return multiton.get(key);
	}
	
	public static synchronized Journal getInstance() {
		return getInstance("Effectu�e");
	}
	
	public static synchronized Journal getErrorInstance() {
		return getInstance("Refus�e");
	}
	
	private StringBuffer log;
	
	protected Journal() {
		this.log = new StringBuffer();
	}

	public void ajouterLog(String log) {
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.log.append("[" + dateFormat.format(d) + "] " + log + "\n");
	}

	@Override
	public String toString() {
		return this.log.toString();
	}	
}
