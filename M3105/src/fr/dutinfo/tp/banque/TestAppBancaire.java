package fr.dutinfo.tp.banque;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAppBancaire {

	@Test
	public void testDepotEffectuee() {
		CompteBancaire c = new CompteBancaire(112233);
		c.deposer(10);
		assertEquals("Depet de 10.0e sur le compte 112233", getLastJournalLog());
	}
	
	@Test
	public void testRetraitEffectue() {
		CompteBancaire c = new CompteBancaire(112233);
		c.deposer(10);
		c.retirer(5);
		assertEquals("Retrait de 5.0e sur le compte 112233", getLastJournalLog());
	}
	
	@Test
	public void testRetraitErreur() {
		CompteBancaire c = new CompteBancaire(112233);
		c.retirer(50);
		assertEquals("/!\\ La banque n'autorise pas de decouvert (112233)", getLastErrorJournalLog());
	}
	
	@Test
	public void testRetraitNegatif() {
		CompteBancaire c = new CompteBancaire(112233);
		c.retirer(-50);
		assertEquals("/!\\ Retrait d'une valeur negative impossible (112233)", getLastErrorJournalLog());
	}
	
	@Test
	public void testDepotNegatif() {
		CompteBancaire c = new CompteBancaire(112233);
		c.deposer(-50);
		assertEquals("/!\\ Depet d'une valeur negative impossible (112233)", getLastErrorJournalLog());
	}
	
	private String getLastJournalLog() {
		String content = Journal.getInstance().toString();
		return content.substring(content.lastIndexOf(']')+2, content.length()-2);
	}
	
	private String getLastErrorJournalLog() {
		String content = Journal.getErrorInstance().toString();
		return content.substring(content.lastIndexOf(']')+2, content.length()-2);
	}

}
