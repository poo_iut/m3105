package fr.dutinfo.tp.comportement;

import java.util.List;

public abstract class CalculatorBillet {

	private CalculatorBillet suivant;
	
	public CalculatorBillet() {	}
	
	public void donnerBillet(WrapperMontant wrapperMontant, List<Couple> propositions, EtatDistributeur etat) {
		if(suivant != null) {
			suivant.donnerBillet(wrapperMontant, propositions, etat);
		}
	}
	
	public void setSuivant(CalculatorBillet suivant) {
		this.suivant = suivant;
	}
	
}
