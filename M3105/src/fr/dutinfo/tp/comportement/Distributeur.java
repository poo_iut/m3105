package fr.dutinfo.tp.comportement;

import java.util.LinkedList;
import java.util.List;

import fr.dutinfo.tp.comportement.calculator.CalculatorBillet10;
import fr.dutinfo.tp.comportement.calculator.CalculatorBillet20;
import fr.dutinfo.tp.comportement.calculator.CalculatorBillet5;
import fr.dutinfo.tp.comportement.calculator.CalculatorBillet50;

public class Distributeur {

	private EtatDistributeur etat;

	private CalculatorBillet5 c5;
	private CalculatorBillet10 c10;
	private CalculatorBillet20 c20;
	private CalculatorBillet50 c50;
	
	public Distributeur(int nb50, int nb20, int nb10) {
		this.etat = new EtatDistributeur();
		recharger(0, nb50, nb20, nb10);
	}
	
	public Distributeur(int nb5, int nb50, int nb20, int nb10) {
		this.etat = new EtatDistributeur();
		recharger(nb5, nb50, nb20, nb10);
	}

	public void recharger(int nb50, int nb20, int nb10) {
		recharger(0, nb50, nb20, nb10);
		this.c5 = new CalculatorBillet5();
		this.c50 = new CalculatorBillet50();
		this.c20 = new CalculatorBillet20();
		this.c10 = new CalculatorBillet10();
	}
	
	public void recharger(int nb5, int nb50, int nb20, int nb10) {
		this.etat.setNb5Disponible(nb5);
		this.etat.setNb50Disponible(nb50);
		this.etat.setNb20Disponible(nb20);
		this.etat.setNb10Disponible(nb10);
	}

	public List<Couple> donnerBilletsPetitesCoupures(int montant) {
		List<Couple> proposition = new LinkedList<Couple>();
		WrapperMontant wrap = new WrapperMontant(montant);
		// Gestion des billets de 50 e
		
		new CalculatorBillet5().donnerBillet(wrap, proposition, this.etat);
		return proposition;
	}
	
	public List<Couple> donnerBillets(int montant) {
		List<Couple> propositions = new LinkedList<Couple>();
		WrapperMontant wrap = new WrapperMontant(montant);
		// Gestion des billets de 50 e
		this.c5.setSuivant(this.c50);
		this.c50.setSuivant(this.c20);
		this.c20.setSuivant(this.c10);
		this.c5.donnerBillet(wrap, propositions, etat);
		return propositions;
	}

	public String toStringProposition(List<Couple> proposition, int montant) {
		StringBuffer res = new StringBuffer();
		res.append("Montant à debiter : " + montant + "e \n");
		for (Couple c : proposition) {
			res.append(c.toString() + '\n');
		}
		res.append("Montant restant de : " + this.montantRestantDe(proposition, montant));
		return res.toString();
	}

	public int montantRestantDe(List<Couple> proposition, int montant) {
		int montantRestantDe = montant;
		for (Couple c : proposition) {
			montantRestantDe -= c.getValeurBillet() * c.getNombreBilletsDelivres();
		}
		return montantRestantDe;
	}
}
