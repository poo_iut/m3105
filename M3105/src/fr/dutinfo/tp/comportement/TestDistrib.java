package fr.dutinfo.tp.comportement;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestDistrib {

	
private Distributeur d ;
	
	@Before
	public void setUp() throws Exception {
		this.d = new Distributeur(10,10,10);
	}

	@After
	public void tearDown() throws Exception {
		this.d = null ;
	}
	
	@Test
	public void test10() {
		List<Couple> proposition = d.donnerBillets(10);
		assertEquals(0,d.montantRestantDe(proposition,10));
		assertEquals(10,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
	}

	@Test
	public void test20() {
		List<Couple> proposition = d.donnerBillets(20);
		assertEquals(0,d.montantRestantDe(proposition,20));
		assertEquals(10,proposition.get(0).getValeurBillet());
		assertEquals(2,proposition.get(0).getNombreBilletsDelivres());
	}

	@Test
	public void test30() {
		List<Couple> proposition = d.donnerBillets(30);
		assertEquals(0,d.montantRestantDe(proposition,30));
		assertEquals(20,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(1).getValeurBillet());
		assertEquals(1,proposition.get(1).getNombreBilletsDelivres());
	}
	
	@Test
	public void test40() {
		List<Couple> proposition = d.donnerBillets(40);
		assertEquals(0,d.montantRestantDe(proposition,40));
		assertEquals(20,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(1).getValeurBillet());
		assertEquals(2,proposition.get(1).getNombreBilletsDelivres());
	}
	
	@Test
	public void test50() {
		List<Couple> proposition = d.donnerBillets(50);
		assertEquals(0,d.montantRestantDe(proposition,50));
		assertEquals(20,proposition.get(0).getValeurBillet());
		assertEquals(2,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(1).getValeurBillet());
		assertEquals(1,proposition.get(1).getNombreBilletsDelivres());
	}
	
	@Test
	public void test60() {
		List<Couple> proposition = d.donnerBillets(60);
		assertEquals(0,d.montantRestantDe(proposition,60));
		assertEquals(20,proposition.get(0).getValeurBillet());
		assertEquals(2,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(1).getValeurBillet());
		assertEquals(2,proposition.get(1).getNombreBilletsDelivres());
	}
	
	@Test
	public void test70() {
		List<Couple> proposition = d.donnerBillets(70);
		assertEquals(0,d.montantRestantDe(proposition,70));
		assertEquals(20,proposition.get(0).getValeurBillet());
		assertEquals(3,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(1).getValeurBillet());
		assertEquals(1,proposition.get(1).getNombreBilletsDelivres());
	}
	
	@Test
	public void test100() {
		List<Couple> proposition = d.donnerBillets(100);
		assertEquals(0,d.montantRestantDe(proposition,100));
		assertEquals(50,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(2,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(1,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test110() {
		List<Couple> proposition = d.donnerBillets(110);
		assertEquals(0,d.montantRestantDe(proposition,110));
		assertEquals(50,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(2,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(2,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test210() {
		List<Couple> proposition = d.donnerBillets(210);
		assertEquals(0,d.montantRestantDe(proposition,210));
		assertEquals(50,proposition.get(0).getValeurBillet());
		assertEquals(2,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(5,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(1,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test310() {
		List<Couple> proposition = d.donnerBillets(310);
		assertEquals(0,d.montantRestantDe(proposition,310));
		assertEquals(50,proposition.get(0).getValeurBillet());
		assertEquals(3,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(7,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(2,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test3000() {
		List<Couple> proposition = d.donnerBillets(3000);
		assertEquals(2200,d.montantRestantDe(proposition,3000));
		assertEquals(50,proposition.get(0).getValeurBillet());
		assertEquals(10,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(10,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(10,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test100Billet5_1() {
		this.d = new Distributeur(1, 10, 10, 10);
		List<Couple> proposition = d.donnerBilletsPetitesCoupures(100);
		assertEquals(5,d.montantRestantDe(proposition,100));
		assertEquals(5,proposition.get(0).getValeurBillet());
		assertEquals(1,proposition.get(0).getNombreBilletsDelivres());
		assertEquals(20,proposition.get(1).getValeurBillet());
		assertEquals(4,proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10,proposition.get(2).getValeurBillet());
		assertEquals(1,proposition.get(2).getNombreBilletsDelivres());
	}
	
	@Test
	public void test100Billet5_2() {
		this.d = new Distributeur(2, 10, 10, 10);
		List<Couple> proposition = d.donnerBilletsPetitesCoupures(100);
		assertEquals(0, this.d.montantRestantDe(proposition, 100));
		assertEquals(5, proposition.get(0).getValeurBillet());
		assertEquals(2, proposition.get(0).getNombreBilletsDelivres());

		assertEquals(20, proposition.get(1).getValeurBillet());
		assertEquals(4, proposition.get(1).getNombreBilletsDelivres());

		assertEquals(10, proposition.get(2).getValeurBillet());
		assertEquals(1, proposition.get(2).getNombreBilletsDelivres());
		//System.out.println(proposition);
	}
	
	@Test
	public void test10Billet5_2() {
		this.d = new Distributeur(2, 10, 10, 10);
		List<Couple> proposition = d.donnerBilletsPetitesCoupures(10);
		assertEquals(0, this.d.montantRestantDe(proposition, 10));
		
		assertEquals(1, proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10, proposition.get(0).getValeurBillet());
	}
	
	@Test
	public void test20Billet5_2() {
		this.d = new Distributeur(2, 10, 10, 10);
		List<Couple> proposition = d.donnerBilletsPetitesCoupures(20);
		assertEquals(0, this.d.montantRestantDe(proposition, 20));
		
		assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
		assertEquals(10, proposition.get(0).getValeurBillet());
	}
	
	@Test
	public void test30Billet5_2() {
		this.d = new Distributeur(2, 10, 10, 10);
		List<Couple> proposition = d.donnerBilletsPetitesCoupures(30);
		
		assertEquals(0, this.d.montantRestantDe(proposition, 30));
		
		assertEquals(2, proposition.get(0).getNombreBilletsDelivres());
		assertEquals(5, proposition.get(0).getValeurBillet());
		
		assertEquals(2, proposition.get(1).getNombreBilletsDelivres());
		assertEquals(10, proposition.get(1).getValeurBillet());
	}
	
	
}
