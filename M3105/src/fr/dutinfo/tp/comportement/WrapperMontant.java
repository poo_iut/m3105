package fr.dutinfo.tp.comportement;

public class WrapperMontant {

	private int montant;

	public WrapperMontant(int montant) {
		super();
		this.montant = montant;
	}
	
	public void decrement(int decrement) {
		this.montant-=decrement;
	}
	
	public int getMontant() {
		return montant;
	}
	
	public void setMontant(int montant) {
		this.montant = montant;
	}
	
}
