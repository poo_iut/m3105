package fr.dutinfo.tp.comportement.calculator;

import java.util.List;

import fr.dutinfo.tp.comportement.CalculatorBillet;
import fr.dutinfo.tp.comportement.Couple;
import fr.dutinfo.tp.comportement.EtatDistributeur;
import fr.dutinfo.tp.comportement.WrapperMontant;

public class CalculatorBillet10 extends CalculatorBillet {

	public CalculatorBillet10() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void donnerBillet(WrapperMontant wrapperMontant, List<Couple> propositions, EtatDistributeur etat) {
		if (wrapperMontant.getMontant() >= 10) {
			int nBillets10 = Math.min(wrapperMontant.getMontant() / 10, etat.getNb10Disponible());
			wrapperMontant.decrement(nBillets10 * 10);
			etat.setNb10Disponible(etat.getNb10Disponible() - nBillets10);
			propositions.add(new Couple(10, nBillets10));
		}
		super.donnerBillet(wrapperMontant, propositions, etat);
	}

}
