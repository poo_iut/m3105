package fr.dutinfo.tp.comportement.calculator;

import java.util.List;

import fr.dutinfo.tp.comportement.CalculatorBillet;
import fr.dutinfo.tp.comportement.Couple;
import fr.dutinfo.tp.comportement.EtatDistributeur;
import fr.dutinfo.tp.comportement.WrapperMontant;

public class CalculatorBillet20 extends CalculatorBillet {

	public CalculatorBillet20() {
	}

	@Override
	public void donnerBillet(WrapperMontant wrapperMontant, List<Couple> propositions, EtatDistributeur etat) {
		if (wrapperMontant.getMontant() > 20) {
			int nBillets20 = 0;
			if (wrapperMontant.getMontant() % 20 == 0) {
				nBillets20 = wrapperMontant.getMontant() / 20 - 1;
			} else {
				nBillets20 = wrapperMontant.getMontant() / 20;
			}
			nBillets20 = Math.min(nBillets20, etat.getNb20Disponible());
			wrapperMontant.decrement(nBillets20 * 20);
			etat.setNb20Disponible(etat.getNb20Disponible() - nBillets20);
			propositions.add(new Couple(20, nBillets20));
		}
		super.donnerBillet(wrapperMontant, propositions, etat);
	}
	
}
