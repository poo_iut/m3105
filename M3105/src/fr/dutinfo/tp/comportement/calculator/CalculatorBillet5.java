package fr.dutinfo.tp.comportement.calculator;

import java.util.List;

import fr.dutinfo.tp.comportement.CalculatorBillet;
import fr.dutinfo.tp.comportement.Couple;
import fr.dutinfo.tp.comportement.EtatDistributeur;
import fr.dutinfo.tp.comportement.WrapperMontant;

public class CalculatorBillet5 extends CalculatorBillet {

	public CalculatorBillet5() {
		setSuivant(new CalculatorBillet50());
	}
	
	@Override
	public void donnerBillet(WrapperMontant wrapperMontant, List<Couple> propositions, EtatDistributeur etat) {
		if(wrapperMontant.getMontant() <= 100
		&& wrapperMontant.getMontant() > 20) {
			if(etat.getNb5Disponible() >= 1) {
				int actuel = Math.min(wrapperMontant.getMontant() / 5, etat.getNb5Disponible());
				int nBillets5 = Math.min(actuel, 2);
				if(nBillets5 > 0) {
					wrapperMontant.decrement(nBillets5 * 5);
					propositions.add(new Couple(5, nBillets5));
				}
			}
		}
		super.donnerBillet(wrapperMontant, propositions, etat);
	}

}
