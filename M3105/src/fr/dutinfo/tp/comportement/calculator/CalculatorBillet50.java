package fr.dutinfo.tp.comportement.calculator;

import java.util.List;

import fr.dutinfo.tp.comportement.CalculatorBillet;
import fr.dutinfo.tp.comportement.Couple;
import fr.dutinfo.tp.comportement.EtatDistributeur;
import fr.dutinfo.tp.comportement.WrapperMontant;

public class CalculatorBillet50 extends CalculatorBillet {

	public CalculatorBillet50() {
		setSuivant(new CalculatorBillet20());
	}

	@Override
	public void donnerBillet(WrapperMontant wrapperMontant, List<Couple> propositions, EtatDistributeur etat) {
		if (wrapperMontant.getMontant() > 50) {
			int nBillets50 = Math.min((int) Math.ceil(wrapperMontant.getMontant() / 2 / 50), etat.getNb50Disponible());
			wrapperMontant.decrement(nBillets50 * 50);
			etat.setNb50Disponible(etat.getNb50Disponible() - nBillets50);
			if (nBillets50 > 0) {
				propositions.add(new Couple(50, nBillets50));
			}
		}
		super.donnerBillet(wrapperMontant, propositions, etat);
	}

}
