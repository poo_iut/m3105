package fr.dutinfo.tp.composite;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.dutinfo.tp.composite.expression.ExpressionDiv;
import fr.dutinfo.tp.composite.expression.ExpressionMoins;
import fr.dutinfo.tp.composite.expression.ExpressionMult;
import fr.dutinfo.tp.composite.expression.ExpressionPlus;

public class CompositeTest {

	/////////////////////////////////////////// Multiplication
	
	public static boolean DEBUG = false;

	@Test
	public void testValeurEntier1() {
		Entier entier = new Entier(7);
		assertEquals(7, entier.evaluate());
	}
	
	@Test
	public void testValeurEntier2() {
		Entier entier = new Entier(0);
		assertEquals(0, entier.evaluate());
	}
	
	@Test
	public void testValeurEntier3() {
		Entier entier = new Entier(-7);
		assertEquals(-7, entier.evaluate());
	}

	/////////////////////////////////////////// Somme
	
	@Test
	public void testExpressionPlus1() {
		ExpressionPlus ep = new ExpressionPlus();
		Entier entier = new Entier(1);
		ep.add(entier);
		assertEquals(1, ep.evaluate());
	}
	
	@Test
	public void testExpressionPlus2() {
		ExpressionPlus ep = new ExpressionPlus();
		Entier entier = new Entier(5);
		ep.add(entier);
		Entier entier2 = new Entier(7);
		ep.add(entier2);
		assertEquals(12, ep.evaluate());
	}
	
	@Test
	public void testExpressionPlus3() {
		ExpressionPlus ep = new ExpressionPlus();
		Entier entier = new Entier(1);
		ep.add(entier);
		Entier entier2 = new Entier(2);
		ep.add(entier2);
		Entier entier3 = new Entier(-3);
		ep.add(entier3);
		assertEquals(0, ep.evaluate());
	}
	
	/////////////////////////////////////////// Multiplication
	
	@Test
	public void testExpressionMult1() {
		ExpressionMult ep = new ExpressionMult();
		Entier entier = new Entier(1);
		ep.add(entier);
		assertEquals(1, ep.evaluate());
	}
	
	@Test
	public void testExpressionMult2() {
		ExpressionMult ep = new ExpressionMult();
		Entier entier = new Entier(5);
		ep.add(entier);
		Entier entier2 = new Entier(7);
		ep.add(entier2);
		assertEquals(35, ep.evaluate());
	}
	
	@Test
	public void testExpressionMult3() {
		ExpressionMult ep = new ExpressionMult();
		Entier entier = new Entier(1);
		ep.add(entier);
		Entier entier2 = new Entier(2);
		ep.add(entier2);
		Entier entier3 = new Entier(-3);
		ep.add(entier3);
		assertEquals(-6, ep.evaluate());
	}
	
	/////////////////////////////////////////// Division
	
	@Test
	public void testExpressionDiv1() {
		ExpressionDiv ep = new ExpressionDiv();
		Entier entier = new Entier(1);
		ep.add(entier);
		assertEquals(1, ep.evaluate());
	}
	
	@Test
	public void testExpressionDiv2() {
		ExpressionDiv ep = new ExpressionDiv();
		Entier entier = new Entier(15);
		ep.add(entier);
		Entier entier2 = new Entier(5);
		ep.add(entier2);
		assertEquals(3, ep.evaluate());
	}
	
	@Test
	public void testExpressionDiv3() {
		ExpressionDiv ep = new ExpressionDiv();
		Entier entier = new Entier(24);
		ep.add(entier);
		Entier entier2 = new Entier(2);
		ep.add(entier2);
		Entier entier3 = new Entier(-3);
		ep.add(entier3);
		assertEquals(-4, ep.evaluate());
	}
	
	///////////////////////////////// Test multiple
	
	@Test
	public void testExpressionSommeSomme() {
		
		Expression expression = new ExpressionPlus();
		expression.add(new Entier(5));
		Expression expression2 = new ExpressionMoins();
		expression2.add(new Entier(1));
		expression2.add(new Entier(2));
		assertEquals(-1, expression2.evaluate());
		expression.add(expression2);
		System.out.println(expression.evaluate());
		assertEquals(4, expression.evaluate());
		
		
	}
}
