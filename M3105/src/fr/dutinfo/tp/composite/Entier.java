package fr.dutinfo.tp.composite;

public class Entier extends Expression{

	private int valeur;
	
	public Entier(int valeur) {
		this.valeur = valeur;
	}

	@Override
	public int evaluate() {
		return this.valeur;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.valeur);
	}
	
	
	@Override
	public void add(Expression expression) {
		// TODO Auto-generated method stub
		//super.add(expression);
	}

	
}
