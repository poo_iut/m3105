package fr.dutinfo.tp.composite;

import java.util.ArrayList;
import java.util.List;

public abstract class Expression {

	protected List<Expression> expressions;
	
	public Expression() {
		this.expressions = new ArrayList<>();
	}
	
	public void add(Expression expression) {
		this.expressions.add(expression);
	}
	
	public abstract int evaluate();
	
	@Override
	public abstract String toString();
}
