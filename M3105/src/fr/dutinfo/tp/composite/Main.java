package fr.dutinfo.tp.composite;

import fr.dutinfo.tp.composite.expression.ExpressionDiv;
import fr.dutinfo.tp.composite.expression.ExpressionMoins;
import fr.dutinfo.tp.composite.expression.ExpressionMult;
import fr.dutinfo.tp.composite.expression.ExpressionPlus;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// calcul de l'expression arihmétique 2 + (3 * (5 -2)) * 6 /6 + 7
		Expression complete = new ExpressionPlus();
		complete.add(new Entier(2));
		Expression milieu = new ExpressionDiv();
		complete.add(milieu);
		complete.add(new Entier(7));
		Expression droitediv = new ExpressionMult();
		milieu.add(droitediv);
		milieu.add(new Entier(6));
		droitediv.add(new Entier(3));
		Expression soustraction = new ExpressionMoins();
		droitediv.add(soustraction);
		droitediv.add(new Entier(6));
		soustraction.add(new Entier(5));
		soustraction.add(new Entier(2));
		System.out.println("2 + (3 * (5 - 2)) * 6 / 6 + 7 = " + complete.evaluate());
		System.out.println(complete.toString());
	}

}
