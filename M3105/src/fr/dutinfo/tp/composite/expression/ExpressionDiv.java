package fr.dutinfo.tp.composite.expression;

import fr.dutinfo.tp.composite.Expression;

public class ExpressionDiv extends Expression{

	@Override
	public int evaluate() {
		if(this.expressions.size() == 0)return 1;
		int somme = this.expressions.get(0).evaluate();
		for (Expression entier : this.expressions) {
			if(entier.equals(this.expressions.get(0))) continue;
			if(entier.evaluate() == 0) {
				return Integer.MAX_VALUE;
			}
			somme /= entier.evaluate();
		}
		return somme;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Expression exp : this.expressions) {
			builder.append(exp.toString()+" / ");
		}
		return "("+builder.replace(builder.length()-2, builder.length(), "").toString()+")";
	}
	
}
