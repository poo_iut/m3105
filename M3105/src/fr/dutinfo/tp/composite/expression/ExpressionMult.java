package fr.dutinfo.tp.composite.expression;

import fr.dutinfo.tp.composite.Expression;

public class ExpressionMult extends Expression{

	@Override
	public int evaluate() {
		int somme = 1;
		for (Expression entier : this.expressions) {
			somme *= entier.evaluate();
		}
		return somme;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Expression exp : this.expressions) {
			builder.append(exp.toString()+" * ");
		}
		return "("+builder.replace(builder.length()-2, builder.length(), "").toString()+")";
	}

}
