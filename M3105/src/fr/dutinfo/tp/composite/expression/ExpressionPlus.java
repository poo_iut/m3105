package fr.dutinfo.tp.composite.expression;

import fr.dutinfo.tp.composite.CompositeTest;
import fr.dutinfo.tp.composite.Expression;

public class ExpressionPlus extends Expression{

	@Override
	public int evaluate() {
		int somme = 0;
		for(Expression entier : this.expressions) {
			if(CompositeTest.DEBUG) System.out.println(entier.evaluate());
			somme+=entier.evaluate();
		}
		return somme;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Expression exp : this.expressions) {
			builder.append(exp.toString()+" + ");
		}
		return "("+builder.replace(builder.length()-2, builder.length(), "").toString()+")";
	}
	
}
