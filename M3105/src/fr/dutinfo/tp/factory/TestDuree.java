package fr.dutinfo.tp.factory;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.dutinfo.tp.factory.duree.Duree;
import fr.dutinfo.tp.factory.duree.DureeEnSecondes;
import fr.dutinfo.tp.factory.fabrique.FabriqueADuree;

public abstract class TestDuree {

	protected FabriqueADuree duree;
	
	@Before
	public abstract void setUp();
	
	@Test
	public void testGetters() {
		Duree d = duree.create(1, 2, 3);
		assertEquals(1,d.getHeures());
		assertEquals(2,d.getMinutes());
		assertEquals(3,d.getSecondes());
	}

	@Test (expected=IllegalArgumentException.class)
	public void testHeuresNegative() {
		duree.create(-1, 2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesNegative() {
		duree.create(1, -2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesSuperieur59() {
		duree.create(1, 60, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesNegative() {
		duree.create(1, 2, -3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesSuperieur59() {
		duree.create(1, 2, 60);
	}
	
	@Test
	public void testEqualsValeursEgales() {
		assertEquals(duree.create(1,2,3),duree.create(1,2,3));
	}
	
	@Test
	public void testEqualsValeursNonEgales() {
		assertNotEquals(duree.create(1,2,3),duree.create(2,2,3));
		assertNotEquals(duree.create(1,2,3),duree.create(1,1,3));
		assertNotEquals(duree.create(1,2,3),duree.create(1,2,2));
	}
	
	@Test
	public void testAjouterUneSeconde() {
		Duree d123 = duree.create(1,2,3);
		d123.ajouterUneSeconde();
		assertEquals(duree.create(1,2,4),d123);
		
		Duree d1259 = duree.create(1,2,59);
		d1259.ajouterUneSeconde();
		assertEquals(duree.create(1,3,0),d1259);
		
		Duree d5959 = duree.create(1,59,59);
		d5959.ajouterUneSeconde();
		assertEquals(duree.create(2,0,0),d5959);
	}
	
	@Test
	public void testCompareTo() {
		assertTrue(duree.create(1,2,4).compareTo(duree.create(1,0,0)) > 0);
		assertEquals(0,duree.create(1,2,4).compareTo(duree.create(1,2,4)));
		assertTrue(duree.create(1,0,0).compareTo(duree.create(1,2,4)) < 0);
	}
	
	@Test 
	public void testToString() {
		assertEquals("1:2:3", duree.create(1,2,3).toString());
	}
	
}
