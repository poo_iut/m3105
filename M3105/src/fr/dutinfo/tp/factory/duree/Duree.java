package fr.dutinfo.tp.factory.duree;

public interface Duree extends Comparable<Duree>{

	public abstract int getHeures();

	public abstract int getMinutes();

	public abstract int getSecondes();

	public abstract void ajouterUneSeconde();

}