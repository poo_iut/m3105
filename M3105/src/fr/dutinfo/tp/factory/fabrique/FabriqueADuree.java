package fr.dutinfo.tp.factory.fabrique;

import fr.dutinfo.tp.factory.duree.Duree;

public interface FabriqueADuree {

	public abstract Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException;
	
}
