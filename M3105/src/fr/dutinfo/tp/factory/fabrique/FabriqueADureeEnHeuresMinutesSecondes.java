package fr.dutinfo.tp.factory.fabrique;

import fr.dutinfo.tp.factory.duree.Duree;
import fr.dutinfo.tp.factory.duree.DureeEnHeuresMinutesSecondes;

public class FabriqueADureeEnHeuresMinutesSecondes implements FabriqueADuree{

	public FabriqueADureeEnHeuresMinutesSecondes() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException {
		return new DureeEnHeuresMinutesSecondes(heures, minutes, secondes);
	}

}
