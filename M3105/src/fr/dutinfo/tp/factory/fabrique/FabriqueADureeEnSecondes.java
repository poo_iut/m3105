package fr.dutinfo.tp.factory.fabrique;

import fr.dutinfo.tp.factory.duree.Duree;
import fr.dutinfo.tp.factory.duree.DureeEnSecondes;

public class FabriqueADureeEnSecondes implements FabriqueADuree {

	public FabriqueADureeEnSecondes() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Duree create(int heures, int minutes, int secondes) throws IllegalArgumentException {
		return new DureeEnSecondes(heures, minutes, secondes);
	}

}
