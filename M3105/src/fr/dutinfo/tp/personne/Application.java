package fr.dutinfo.tp.personne;

public class Application {

	public static void main(String[] args) {
		Personne[] listePersonnes = { new Personne("Sédes", "Florence"),
									  new Personne("Viallet", "Fabienne"),
									  new Personne("Julien", "Christine"),
									  new Personne("Percebois", "Christian"),
									  new Personne("Leblanc", "Hervé"),
									  // Ajout
									  new Personne("Fouchou-lapeyrade", "Guilhem")};
		
		Observer etatCivilToulouse = new ObserverEtatCivil();
		Observer hauteGaronnePTT = new ObserverPTT();
		
		for (Personne p : listePersonnes) {
			p.attach(etatCivilToulouse);
			p.attach(hauteGaronnePTT);
		}
			
		listePersonnes[3].ajoutPrénom("Augustin");
		listePersonnes[4].ajoutPrénom("Just");
		listePersonnes[4].setNom("Lerouge");
		System.out.println("==================");
		// Ajout
		listePersonnes[5].setNom("Fouchou");
		System.out.println("==================");
		listePersonnes[5].ajoutPrénom("Eliot");
	}

}
