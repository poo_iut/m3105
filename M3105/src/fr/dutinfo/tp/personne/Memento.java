package fr.dutinfo.tp.personne;

import java.util.LinkedList;
import java.util.List;

public class Memento implements Cloneable {

	private String nom;

	private List<String> prénoms;

	public Memento(){
		this.nom = null;
		this.prénoms = new LinkedList<String>();
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPremierPrénom() {
		if (this.prénoms.size() > 0)
			return this.prénoms.get(0);
		else
			return null;
	}
	
	public String getDeuxiémePrénom() {
		if (this.prénoms.size() > 1)
			return this.prénoms.get(1);
		else
			return null;
	}
	
	public String getTroisiémePrénom() {
		if (this.prénoms.size() > 2)
			return this.prénoms.get(2);
		else
			return null;
	}
	
	public boolean ajoutPrénom(String prénom) {
		if (this.prénoms.size() < 3){
			this.prénoms.add(prénom);
			return true;
		}
		return false;
	}
	
	public Memento clone() {
		Memento memento = null;
		try {
			memento = (Memento) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		memento.prénoms = new LinkedList<String>();
		for (String s : this.prénoms) {
			memento.prénoms.add(s);
		}
		return memento;
	}

}
