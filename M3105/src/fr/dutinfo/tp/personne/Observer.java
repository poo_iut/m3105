package fr.dutinfo.tp.personne;

public interface Observer {
	
	public void update(Subject s);
	
}
