package fr.dutinfo.tp.personne;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPersonne {

	private Personne personne;
	private TestObserver observer;

	@Before
	public void setup() {
		this.personne = new Personne("Cayre", "Fabien");
		this.observer = new TestObserver();
		this.personne.attach(this.observer);
	}

	@After
	public void tearDown() {
		this.personne.detach(this.observer);
		this.personne = null;
	}

	@Test
	public void testObserverUpdate() {
		String nouveauNom = "Chirac";
		this.personne.setNom(nouveauNom);
		assertEquals(true, this.observer.hasChange());
	}
	
	@Test
	public void testObserverChangerNom() {
		String nouveauNom = "Chirac";
		this.personne.setNom(nouveauNom);
		assertEquals("Chirac", this.observer.nom);
	}
	
	@Test
	public void testObserverAjouterPrénom() {
		String nouveauPrénom = "Michel";
		this.personne.ajoutPrénom(nouveauPrénom);
		assertEquals("Michel", this.observer.prenoms.get(0));
	}
	
	@Test
	public void testObserverAjouterDeuxPrénom() {
		String prénom1 = "Michel";
		String prénom2 = "Jean";
		this.personne.ajoutPrénom(prénom1);
		assertEquals("Michel", this.observer.prenoms.get(0));
		this.personne.ajoutPrénom(prénom2);
		assertEquals("Jean", this.observer.prenoms.get(0));
	}

	private class TestObserver implements Observer {

		private String nom;
		private LinkedList<String> prenoms;
		private boolean change;

		@Override
		public void update(Subject s) {
			Personne p = (Personne) s;
			if (!p.getAfter().getNom().equals(p.getBefore().getNom())) {
				this.nom = p.getAfter().getNom();
				this.change = true;
			}
			prénoms(p);
		}

		private void prénoms(Personne p) {
			this.prenoms = new LinkedList<String>();
			if (!p.getAfter().getPremierPrénom().equals(p.getBefore().getPremierPrénom())) {
				this.prenoms.addLast(p.getAfter().getPremierPrénom());
				System.out.println(this.prenoms);
				this.change = true;
			}
			if (p.getAfter().getDeuxiémePrénom() != null)
				if (!p.getAfter().getDeuxiémePrénom().equals(p.getBefore().getDeuxiémePrénom())) {
					this.prenoms.addLast(p.getAfter().getDeuxiémePrénom());
					this.change = true;
				}
			if (p.getAfter().getTroisiémePrénom() != null) {
				if (!p.getAfter().getTroisiémePrénom().equals(p.getBefore().getTroisiémePrénom())) {
					this.prenoms.addLast(p.getAfter().getTroisiémePrénom());
					this.change = true;
				}
			}
		}

		public boolean hasChange() {
			return this.change;
		}

		public void reset() {
			this.change = false;
		}

	}

}
