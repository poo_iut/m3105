package fr.dutinfo.tp.singleton;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;

public class SingletonTest {

	
	@Test
	public void testSingletonNotNull() {
		assertNotNull(Singleton.getInstance());
	}
	
	@Test
	public void testSingletonInstanceof() {
		assertTrue(Singleton.getInstance() instanceof Singleton);
	}
	
	
	public void testSingletonSameInstance() {
		assertSame(Singleton.getInstance(), Singleton.getInstance());
	}

}
