package fr.dutinfo.tp.tp2;

public class Arbre extends ObjectGraphique {

	private int hauteur;
	private String couleurTronc;
	private String couleurFeuilles;
	
	
	public Arbre(double coordX, double coordY, int hauteur, String couleurTronc, String couleurFeuilles) {
		super(coordX, coordY);
		this.hauteur = hauteur;
		this.couleurTronc = couleurTronc;
		this.couleurFeuilles = couleurFeuilles;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public void setCouleurTronc(String couleurTronc) {
		this.couleurTronc = couleurTronc;
	}

	public void setCouleurFeuilles(String couleurFeuilles) {
		this.couleurFeuilles = couleurFeuilles;
	}

	@Override
	public String toString() {
		return "Arbre [hauteur=" + hauteur + ", couleurTronc=" + couleurTronc + ", couleurFeuilles=" + couleurFeuilles
				+ ", toString()=" + super.toString() + "]";
	}
	
	@Override
	public Arbre clone() {
		return (Arbre) super.clone();
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0 == null) return false;
		if(!(arg0 instanceof Arbre)) return false;
		if(arg0 == this) return true;
		Arbre a = (Arbre) arg0;
		return 
			   this.couleurFeuilles == a.couleurFeuilles
			&& this.couleurTronc == a.couleurTronc
			&& this.hauteur == a.hauteur
			&& super.equals(a);
	}
	

	
}
