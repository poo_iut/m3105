package fr.dutinfo.tp.tp2;

public class Balancoire extends ObjectGraphique{

	private String couleur;
	private int hauteur;
	private int nbSieges;
	
	

	public Balancoire(double coordX, double coordY, String couleur, int hauteur, int nbSieges) {
		super(coordX, coordY);
		this.couleur = couleur;
		this.hauteur = hauteur;
		this.nbSieges = nbSieges;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	public void setNbSieges(int nbSieges) {
		this.nbSieges = nbSieges;
	}

	@Override
	public Balancoire clone() {
		return (Balancoire) super.clone();
	}

	@Override
	public String toString() {
		return "Balancoire [couleur=" + couleur + ", hauteur=" + hauteur + ", nbSieges=" + nbSieges + ", toString()="
				+ super.toString() + "]";
	}
	
	
}
