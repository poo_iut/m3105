package fr.dutinfo.tp.tp2;

public class Banc extends ObjectGraphique {

	private String couleur;
	private int hauteur;
	private int largeur;
	
	public Banc(double coordX, double coordY, String couleur, int hauteur, int largeur) {
		super(coordX, coordY);
		this.couleur = couleur;
		this.hauteur = hauteur;
		this.largeur = largeur;
	}
	
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	@Override
	public Banc clone() {
		return (Banc) super.clone();
	}
	
	@Override
	public String toString() {
		return "Banc [couleur=" + couleur + ", hauteur=" + hauteur + ", largeur=" + largeur + ", toString()="
				+ super.toString() + "]";
	}
	
	
}
