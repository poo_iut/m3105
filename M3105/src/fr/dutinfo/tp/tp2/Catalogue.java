package fr.dutinfo.tp.tp2;

import java.util.List;

public class Catalogue {

	private static Catalogue catalogue;
	
	public static Catalogue getCatalogue() {
		if(catalogue == null) {
			catalogue = new Catalogue();
		}
		return catalogue;
	}
	
	private final Immeuble unImmeuble;
	private final Arbre unArbre;
	private final Balancoire uneBalancoire;
	private final Banc unBanc;
	private final Toboggan unToboggan;
	private final Tourniquet unTourniquet;
	private final Cite uneCite;
	
	private Catalogue() {
		this.unImmeuble = new Immeuble(0, 0, "Blanc", 4, 3);
		this.unArbre = new Arbre(0, 0, 8, "Brun", "Vert");
		this.uneBalancoire = new Balancoire(0, 0, "Vert", 3, 2);
		this.unBanc = new Banc(0, 0, "Brun", 1, 2);
		this.unToboggan = new Toboggan(0, 0, "Jaune", 2, 4);
		this.unTourniquet = new Tourniquet(0, 0, "Rouge", 1, 3);
		this.uneCite = new Cite("Cite des alouettes");
		initializeCite();
	}
	
	private void initializeCite() {
		List<ObjectGraphique> data = this.uneCite.getData();
		data.add(this.unImmeuble.clone());
		data.add(this.unImmeuble.clone());
		data.add(this.unBanc.clone());
		data.add(this.unBanc.clone());
		data.add(this.unBanc.clone());
		data.add(this.uneBalancoire.clone());
		data.add(this.unToboggan.clone());
		data.add(this.unArbre.clone());
		data.add(this.unArbre.clone());
	}
	
	public Arbre getUnArbre() {
		return this.unArbre.clone();
	}
	
	public Immeuble getUnImmeuble() {
		return this.unImmeuble.clone();
	}

	public Balancoire getUneBalancoire() {
		return this.uneBalancoire.clone();
	}

	public Banc getUnBanc() {
		return this.unBanc.clone();
	}

	public Toboggan getUnToboggan() {
		return this.unToboggan.clone();
	}

	public Tourniquet getUnTourniquet() {
		return this.unTourniquet.clone();
	}

	public Cite getUneCite() {
		return uneCite.clone();
	}

	

}
