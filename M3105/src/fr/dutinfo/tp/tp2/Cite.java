package fr.dutinfo.tp.tp2;

import java.util.LinkedList;

public class Cite implements Cloneable{

	private LinkedList<ObjectGraphique> data;
	private String nom;
	
	public Cite(String nom) {
		this.nom = nom;
		this.data = new LinkedList<ObjectGraphique>();
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public LinkedList<ObjectGraphique> getData() {
		return data;
	}
	
	@Override
	public String toString() {
		return "Cit� [data=" + data + ", nom=" + nom + "]";
	}

	@Override
	public Cite clone() {
		try {
			Cite cite = (Cite) super.clone();
			cite.data = new LinkedList<ObjectGraphique>();
			for(ObjectGraphique obj : this.data) {
				cite.data.add(obj.clone());
			}
			//this.data.stream().map(ObjectGraphique::clone).forEach(cit�.data::add);
			return cite;
		} catch (Exception e) {
			return null;
		}
	}
	
	
	
	
}
