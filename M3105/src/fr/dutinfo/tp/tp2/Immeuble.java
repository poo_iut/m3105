package fr.dutinfo.tp.tp2;

public class Immeuble extends ObjectGraphique {

	private String couleur;
	private int nbEtage;
	private int hauteurEtage;
	
	public Immeuble(double coordX, double coordY, String couleur, int nbEtage, int hauteurEtage) {
		super(coordX, coordY);
		this.couleur = couleur;
		this.nbEtage = nbEtage;
		this.hauteurEtage = hauteurEtage;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public void setNbEtage(int nbEtage) {
		this.nbEtage = nbEtage;
	}

	public void setHauteurEtage(int hauteurEtage) {
		this.hauteurEtage = hauteurEtage;
	}

	@Override
	public Immeuble clone() {
		return (Immeuble) super.clone();
	}
	
	@Override
	public String toString() {
		return "Immeuble [couleur=" + couleur + ", nbEtage=" + nbEtage + ", hauteurEtage=" + hauteurEtage
				+ ", toString()=" + super.toString() + "]";
	}

	@Override
	public boolean equals(Object arg0) {
		if(arg0 == null) return false;
		if(!(arg0 instanceof Immeuble)) return false;
		if(arg0 == this) return true;
		Immeuble a = (Immeuble) arg0;
		return 
			   this.couleur == a.couleur
			&& this.nbEtage == a.nbEtage
			&& this.hauteurEtage == a.hauteurEtage
			&& super.equals(a);
	}
	
	
	
}
