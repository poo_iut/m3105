package fr.dutinfo.tp.tp2;

public abstract class ObjectGraphique implements Cloneable{

	private double coordX;
	private double coordY;
	
	public ObjectGraphique(double coordX, double coordY) {
		this.coordX = coordX;
		this.coordY = coordY;
	}
	
	public void translate(double dx, double dy) {
		this.coordX+=dx;
		this.coordY+=dy;
	}

	@Override
	public String toString() {
		return "ObjectGraphique [coordX=" + coordX + ", coordY=" + coordY + "]";
	}
	
	@Override
	public ObjectGraphique clone() {
		try {
			return (ObjectGraphique) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof ObjectGraphique)) return false;
		if(obj == this) return true;
		ObjectGraphique a = (ObjectGraphique) obj;
		return
			this.coordX == a.coordX && this.coordY == a.coordY;
	}
	
}
