package fr.dutinfo.tp.tp2;

import java.util.Arrays;
import java.util.List;

public class PremierPaysage {

	public static void main(String[] args) {
		
		Catalogue cata = Catalogue.getCatalogue();
		List<ObjectGraphique> paysage
			= Arrays.asList(
					cata.getUnImmeuble(),
					cata.getUnImmeuble(),
					cata.getUnArbre(),
					cata.getUnBanc(),
					cata.getUneBalancoire(),
					cata.getUnToboggan(),
					cata.getUnTourniquet()
					);
		for(ObjectGraphique o : paysage) {
			o.translate(Math.random() * 10, Math.random() * 10);
			System.out.println(o);
		}
		
	}

}
