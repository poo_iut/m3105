package fr.dutinfo.tp.tp2;

public class TestCite {

	public static void main(String[] args) {
		Catalogue catalogue = Catalogue.getCatalogue();
		Cite cite = catalogue.getUneCite();
		Immeuble test = (Immeuble) cite.getData().get(0);
		test.setCouleur("Rouge");
		test.translate(3.3, 4.4);
		System.out.println(cite);
		Cite uneAutreCite = catalogue.getUneCite();
		System.out.println(uneAutreCite);
	}

}
