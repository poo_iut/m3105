package fr.dutinfo.tp.tp2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

public class TestClonage {

	@Test
	public void testEqualImmeuble() {
		Immeuble m = new Immeuble(0, 0, "blanc", 5, 2);
		Immeuble cloned = m.clone();
		assertNotSame(cloned, m);
		assertEquals(cloned, m);
	}

	@Test
	public void testEqualArbre() {
		Arbre m = new Arbre(0, 0, 5, "blanc", "bleu");
		Arbre cloned = (Arbre) m.clone();
		assertNotSame(cloned, m);
		assertEquals(cloned, m);
	}

}
