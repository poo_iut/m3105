package fr.dutinfo.tp.tp2;

public class Tourniquet extends ObjectGraphique {

	private String couleur;
	private int hauteur;
	private int diametre;
	
	public Tourniquet(double coordX, double coordY, String couleur, int hauteur, int diametre) {
		super(coordX, coordY);
		this.couleur = couleur;
		this.hauteur = hauteur;
		this.diametre = diametre;
	}
	
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	public void setDiametre(int diametre) {
		this.diametre = diametre;
	}

	@Override
	public Tourniquet clone() {
		return (Tourniquet) super.clone();
	}
	
	@Override
	public String toString() {
		return "Tourniquet [couleur=" + couleur + ", hauteur=" + hauteur + ", diametre=" + diametre + ", toString()="
				+ super.toString() + "]";
	}
	
	
}
